###########################################################
#
###########################################################


####################################################################################################################################################
# 															   Table of Contents
####################################################################################################################################################

## Base
  # path_loader

path_loader <- function(file) {

  ## Iterate through and set path locals into global environment
  ## where full paths (root = J:/blah") are set directly and recursive
  ## paths (ie blah_path = root, /blah") are set through replacement

  ## Set paths
  paths <- fread(file)
  for (j in 2:ncol(paths)) {
    for (i in 1:nrow(paths)) {
      obj <- paths[i, 1, with=F] %>% unlist
      path <- paths[i, j, with=F] %>% unlist
      if (path != "" & !is.na(path)) assign(obj, path, envir=globalenv())
      if (grepl("[,]", path)) {
        replace <- gsub("[,].*$", "", path) %>% gsub(" ", "", .)
        end <- gsub(".*[,]", "", path) %>% gsub(" ", "", .)
        path <- paste0(get(replace), end)
        assign(obj, path, envir=globalenv())
      }
    }
  }

  ## Done
  print("Paths loaded")

}
